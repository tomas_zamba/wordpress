const { mix } = require('laravel-mix')

/*
 |--------------------------------------------------------------------------
 | Mix Asset Management
 |--------------------------------------------------------------------------
 |
 | Mix provides a clean, fluent API for defining some Webpack build steps
 | for your Laravel application. By default, we are compiling the Sass
 | file for the application as well as bundling up all the JS files.
 |
 */

mix
    .webpackConfig({
        module: {
            rules: [
                {
                    enforce: 'pre',
                    exclude: /node_modules/,
                    loader: 'eslint-loader',
                    test: /\.(js|vue)?$/,
                    options: {
                        fix: true,
                    },
                },
            ],
        },
    })
    .js('wp-content/themes/uxfaktor/src/js/app.js', 'wp-content/themes/uxfaktor/app.js')
    .less('wp-content/themes/uxfaktor/src/less/style.less', 'wp-content/themes/uxfaktor/style.css')
    .copyDirectory('wp-content/themes/uxfaktor/src/images', 'wp-content/themes/uxfaktor/assets/images')
    .copyDirectory('wp-content/themes/uxfaktor/src/svg', 'wp-content/themes/uxfaktor/assets/svg')

if (mix.inProduction()) {
    mix.version()
}
