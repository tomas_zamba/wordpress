<?php

// Used to VERSION JS and CSS files
define( 'VERSION_UXFAKTOR', '0.0.0' );

function uxfaktor_setup() {
    load_theme_textdomain( 'uxfaktor' );

    // Add default posts and comments RSS feed links to head.
    add_theme_support( 'automatic-feed-links' );

    /*
	 * Let WordPress manage the document title.
	 * By adding theme support, we declare that this theme does not use a
	 * hard-coded <title> tag in the document head, and expect WordPress to
	 * provide it for us.
	 */
    add_theme_support( 'title-tag' );

    /*
	 * Enable support for Post Thumbnails on posts and pages.
	 *
	 * @link https://developer.wordpress.org/themes/functionality/featured-images-post-thumbnails/
	 */
    add_theme_support( 'post-thumbnails' );

    add_image_size( 'uxfaktor-featured-image', 2000, 1200, true );

    add_image_size( 'uxfaktor-thumbnail-avatar', 100, 100, true );

    // Set the default content width.
    $GLOBALS['content_width'] = 525;

    // This theme uses wp_nav_menu() in one location.
    register_nav_menus( array(
        'primary'    => __( 'Primary Menu', 'uxfaktor' ),
    ) );

    /*
	 * Enable support for Post Formats.
	 *
	 * See: https://codex.wordpress.org/Post_Formats
	 */
    add_theme_support( 'post-formats', array(
        'aside',
        'image',
        'video',
        'quote',
        'link',
        'gallery',
        'audio',
    ) );

    /*
	 * Switch default core markup for search form, comment form, and comments
	 * to output valid HTML5.
	 */
    add_theme_support( 'html5', array(
        'search-form',
        'comment-form',
        'comment-list',
        'gallery',
        'caption',
    ) );

    // Add theme support for Custom Logo.
    add_theme_support( 'custom-logo', array(
        'width'      => 250,
        'height'     => 250,
        'flex-width' => true,
    ) );
}
add_action( 'after_setup_theme', 'uxfaktor_setup' );

/**
 * Enqueue scripts and styles.
 */
function uxfaktor_scripts() {

    /**
     * Remove WordPress embed script
     */
    wp_deregister_script( 'wp-embed' );

    /**
     * Remove WooCommerce scripts
     */
    wp_dequeue_style( 'woocommerce_frontend_styles' );
    wp_dequeue_style( 'woocommerce-general' );
    wp_dequeue_style( 'woocommerce-layout' );
    wp_dequeue_style( 'woocommerce-smallscreen' );
    wp_dequeue_style( 'woocommerce_fancybox_styles' );
    wp_dequeue_style( 'woocommerce_chosen_styles' );
    wp_dequeue_style( 'woocommerce_prettyPhoto_css' );
    wp_dequeue_script( 'selectWoo' );
    wp_deregister_script( 'selectWoo' );
    wp_dequeue_script( 'wc-add-payment-method' );
    wp_dequeue_script( 'wc-lost-password' );
    wp_dequeue_script( 'wc_price_slider' );
    wp_dequeue_script( 'wc-single-product' );
    wp_dequeue_script( 'wc-add-to-cart' );
    wp_dequeue_script( 'wc-cart-fragments' );
    wp_dequeue_script( 'wc-credit-card-form' );
    wp_dequeue_script( 'wc-checkout' );
    wp_dequeue_script( 'wc-add-to-cart-variation' );
    wp_dequeue_script( 'wc-single-product' );
    wp_dequeue_script( 'wc-cart' );
    wp_dequeue_script( 'wc-chosen' );
    wp_dequeue_script( 'woocommerce' );
    wp_dequeue_script( 'prettyPhoto' );
    wp_dequeue_script( 'prettyPhoto-init' );
    wp_dequeue_script( 'jquery-blockui' );
    wp_dequeue_script( 'jquery-placeholder' );
    wp_dequeue_script( 'jquery-payment' );
    wp_dequeue_script( 'fancybox' );
    wp_dequeue_script( 'jqueryui' );

    /**
     * Remove emoji styles and scripts
     */
    remove_action( 'wp_head', 'print_emoji_detection_script', 7 );
    remove_action( 'wp_print_styles', 'print_emoji_styles' );

    /**
     * Add custom styles
     */
    wp_register_style( 'uxfaktor-style', get_stylesheet_uri(), array(), VERSION_UXFAKTOR, 'all' );
    wp_enqueue_style( 'uxfaktor-style' );

    /**
     * Add custom scripts
     */
    wp_register_script( 'uxfaktor-app', get_theme_file_uri( './app.js' ), array(), VERSION_UXFAKTOR, true );
    wp_enqueue_script( 'uxfaktor-app' );
}

add_action('wp_enqueue_scripts', 'uxfaktor_scripts');

function get_uxfaktor_post_card($index) { ?>
    <article>
        <div><?php echo $index; ?></div>
        <header>
            <h2 class="entry-title"><?php the_title(); ?></h2>
        </header>
        <div class="entry-content">
            <?php the_excerpt(); ?>
        </div>
    </article>
<?php
}
