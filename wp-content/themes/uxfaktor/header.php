<!DOCTYPE html>
<html <?php language_attributes(); ?>>
<head>
    <meta charset="<?php bloginfo( 'charset' ); ?>">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="profile" href="http://gmpg.org/xfn/11">
    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Lato:300,400,700|Montserrat:300,400,600,700&subset=latin-ext">

    <style>
        body, html { margin: 0; padding: 0; }
        html { background-color: #fff; }
        body { background-color: #eff1f3; border: 20px solid #fff; }
    </style>

    <?php wp_head(); ?>
</head>

<body <?php body_class(); ?>>

    <header>
        <?php if ( is_front_page() ) : ?>
            <h1 class="site-title">
                <a href="<?php echo esc_url( home_url( '/' ) ); ?>" rel="home">
                    <?php bloginfo( 'name' ); ?>
                </a>
            </h1>
        <?php else : ?>
            <p class="site-title">
                <a href="<?php echo esc_url( home_url( '/' ) ); ?>" rel="home">
                    <?php bloginfo( 'name' ); ?>
                </a>
            </p>
        <?php endif; ?>
    </header>
