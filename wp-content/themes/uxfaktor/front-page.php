<?php get_header(); ?>

    <div id="primary" class="content-area">
        <main id="main" class="site-main">

            FRONTPAGE

            <br>
            <?php bloginfo( 'title' ); ?>
            <br>
            <p>
                <?php bloginfo( 'description' ); ?>
            </p>
            <p>
                While it was just a TV show, that little speech at the beginning of the original Star Trek show really
                did do a good job of capturing our feelings about space.
            </p>

            <section>
                LATEST BLOGS
            </section>

            <?php
            $index = 1;
            if ( have_posts() ) :
                while ( have_posts() ) : the_post();
                    get_uxfaktor_post_card( $index );
                    $index ++;
                endwhile;
            endif; ?>

        </main><!-- #main -->
    </div><!-- #primary -->

<?php get_footer();
