<?php get_header(); ?>

    <div class="wrap">
        <?php if ( is_home() && ! is_front_page() ) : ?>
            <header class="page-header">
                <h1 class="page-title"><?php single_post_title(); ?></h1>
            </header>
        <?php else : ?>
            <header class="page-header">
                <h2 class="page-title"><?php _e( 'Posts', 'uxfaktor' ); ?></h2>
            </header>
        <?php endif; ?>

        <div id="primary" class="content-area">
            <main id="main" class="site-main" role="main">

                <?php
                if ( have_posts() ) :

                    while ( have_posts() ) :

                        the_post();

                        get_template_part( 'template-parts/post/content', get_post_format() );

                    endwhile;

                    the_posts_pagination(
                        array(
                            'prev_text'          => '<span class="screen-reader-text">' . __( 'Previous page', 'uxfaktor' ) . '</span>',
                            'next_text'          => '<span class="screen-reader-text">' . __( 'Next page', 'uxfaktor' ) . '</span>',
                            'before_page_number' => '<span class="meta-nav screen-reader-text">' . __( 'Page', 'uxfaktor' ) . ' </span>',
                        )
                    );

                else :

                    get_template_part( 'template-parts/post/content', 'none' );

                endif;
                ?>

            </main>
        </div>

        <?php get_sidebar(); ?>

    </div>

<?php get_footer();
