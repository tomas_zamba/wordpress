import 'babel-polyfill'

import Vue from 'vue'

/* eslint-disable */
const appElement = document.getElementById('app')
if (appElement) {
    const app = new Vue({
        el: '#app',
        mounted() {
            console.info('ZeoCMS')
        },
    })
} else {
    console.error('Add #app element to page')
}
/* eslint-enable */
