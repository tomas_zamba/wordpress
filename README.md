#WordPress

### Frontend
```
yarn
```

####Development
```
yarn run watch
```

####Production
```
yarn run prod
```

### `wp-config.php` setup for Odyzeo localhost `PROJECT_NAME`
```
define('WP_HOME','http://localhost/PROJECT_NAME');
define('WP_SITEURL','http://localhost/PROJECT_NAME');
```

### `.htaccess` file for `PROJECT_NAME`
#### localhost
```
# BEGIN WordPress
<IfModule mod_rewrite.c>
RewriteEngine On
RewriteBase /PROJECT_NAME/
RewriteRule ^index\.php$ - [L]
RewriteCond %{REQUEST_FILENAME} !-f
RewriteCond %{REQUEST_FILENAME} !-d
RewriteRule . /PROJECT_NAME/index.php [L]
</IfModule>
# END WordPress
```

####Production
Copy `.htaccess-production` file to `.htaccess` and replace `PROJECT_DOMAIN` with project domain name. 
```
...
<IfModule mod_rewrite.c>
RewriteEngine On
RewriteCond %{HTTP_HOST} ^www\.
RewriteRule ^ https://PROJECT_DOMAIN%{REQUEST_URI} [R=permanent,L,QSA]
RewriteCond %{HTTP:X-Forwarded-Proto} !=https
RewriteRule ^ https://PROJECT_DOMAIN.org%{REQUEST_URI} [R=permanent,L,QSA]

RewriteBase /
RewriteRule ^index\.php$ - [L]
RewriteCond %{REQUEST_FILENAME} !-f
RewriteCond %{REQUEST_FILENAME} !-d
RewriteRule . /index.php [L]
</IfModule>
...
```

### `webpack.mix.js` set `THEME_NAME`, should be the same as `PROJECT_NAME`
```
.js('wp-content/themes/THEME_NAME/src/js/app.js', 'wp-content/themes/THEME_NAME/app.js')
```

### Deployment
####PhpStorm / WebStorm
Deployment is set in `.idea` configuration files

####Odyzeoclients
Subfolder should be same as `THEME_NAME` and `PROJECT_NAME`
```
/app
/log
/sub/PROJECT_NAME/
```

####Production on WebSupport
```
/logs
/sub
/web/WORDPRESS_PROJECT
```
